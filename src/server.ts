import * as fs from "fs";
import Excel from "exceljs";
import * as path from "path";

const main = async () => {
  const lastArray: { title: string; type: string[] }[] = [];
  const filePath = path.resolve(
    "/Users/ulanbatyrbekov/projects/upload_data/labs.xlsx"
  );
  const workbook = new Excel.Workbook();
  const content = await workbook.xlsx.readFile(filePath);

  for (const worksheets of content.worksheets) {
    // console.log(worksheets.columns.length);
    console.log(worksheets.rowCount);
    worksheets.eachRow(
      {
        includeEmpty: false,
      },
      (row, rowNumber) => {
        const firstCell = row.getCell(1);
        const firstText = firstCell.text;
        if (firstText !== "" && !Number.isNaN(Number(firstText))) {
          const cell = row.getCell(2);
          console.log(cell.text);
          lastArray.push({
            title: cell.text,
            type: [],
          });
        }
      }
    );
  }
  fs.writeFile("test.json", JSON.stringify(lastArray), function (err) {
    if (err) {
      console.log(err);
    }
  });
};

main();
